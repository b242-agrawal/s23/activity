let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log("Pikachu! I choose you");
	}
}
console.log(trainer.name);
console.log(trainer['pokemon']);
trainer.talk();

function createPokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;
	this.tackle = function(target){
		target.health -= this.attack;
		if (target.health <= 0){this.faint(target.name)}
	};
	this.faint = function(name){
		console.log(name + " has fainted")
	};
}

let pikachu = new createPokemon("Pikachu", 12);
let geodude = new createPokemon("Geodude", 8);
let mewtwo = new createPokemon("Mewtwo", 100);
mewtwo.tackle(geodude);